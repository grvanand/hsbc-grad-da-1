package com.hsbc.da1.exception;
import java.util.*;

public class HashSetDemo {

		public static void main(String[] args) {
			Set<Integer> set = new HashSet<>();
			set.add(23);
			set.add(23);
			set.add(23);
			set.add(23);
			set.add(33);
			set.add(23);
			set.add(23);
			set.add(44);
			set.add(55);
			
			System.out.printf("Total number of elements %d %n", set.size());
			System.out.println(set.contains(23));
			
			Iterator<Integer> it = set.iterator();
			
			while(it.hasNext()) {
				int value = it.next();
				System.out.println("The value is " + value);
			}
			System.out.println("Size of the set : " + set.size());
			
		}
}
