package com.hsbc.da1.exception;

public class ExceptionDemo {
	
	public static void main(String[] args) {
		
		int a = 10;
		int b = 0;
		
		int array[] = {35, 67, 89 , 56};
		
		try {
			System.out.println(a + " / " + b + " = " + a/b );
			System.out.println("Last element of array = " + array[array.length]);
		}
		catch(ArithmeticException ae) {
			System.out.println("Cannot divide by 0");
		} catch (ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Cannot fetch the elements; unreachable");
		}
		finally {
			System.out.println("Thank you for your time!");
		}
	}

}
