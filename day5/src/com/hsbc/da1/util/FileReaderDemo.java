package com.hsbc.da1.util;

import java.io.*;

public class FileReaderDemo {
	
	public static void main(String[] args) {
		
		File textFile = new File("G://hsbc-grads-da-1//hsbc-training//test.txt");
		
		//System.out.printf("Absolute Path %s %n", textFile.getAbsoutePath());
		//System.out.printf("Is a directory %s %n", textFile.isDirectory());
		//System.out.printf("Is a file %s %n", textFile.isFile());
		
		readFile(textFile);
		writeFile(textFile, "G://hsbc-grads-da-1//hsbc-training//destination.txt");
}
	
	public static void writeFile(File textFile, String destFile) {
		
		try (
				BufferedReader bufferedReader = new BufferedReader(new FileReader (textFile));
				BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(destFile)));
			){
			boolean endOfFile = false;
			while( ! endOfFile) {
				String line = bufferedReader.readLine();
				if(line != null) {
					bufferedWriter.append(line);
					bufferedWriter.newLine();
				} else {
					endOfFile = true;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static void readFile(File textFile) {
		
			/*
			try (FileReader fileReader = new FileReader(textFile)) {
				boolean endOfFile = false;
				while( ! endOfFile) {
					int character = fileReader.read();
					if(character != -1) {
						char c = (char)character;
						System.out.print(c);
					} else {
						endOfFile = true;
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			*/
			
			try (BufferedReader bufferedReader = new BufferedReader(new FileReader (textFile))) {
				boolean endOfFile = false;
				while( ! endOfFile) {
					String line = bufferedReader.readLine();
					if(line != null) {
						System.out.print(line);
					} else {
						endOfFile = true;
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}//enhanced than above code
		}
	
	}
