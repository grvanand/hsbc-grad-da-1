package com.hsbc.da1.threadsAssignment;

public class PhotosClient {
	
	public static void main(String[] args) {
		
		System.out.println("Thread when the program is starting : " + Thread.currentThread().getName());
		
		System.out.println("=========================================================");
		
		Runnable googlePhotos = new GooglePhotos();
		Runnable flickr = new Flickr();
		Runnable picassa = new Picassa();
		
		Thread threadGooglePhotos = new Thread (googlePhotos);
		threadGooglePhotos.setName("Google Photos");
		//threadGooglePhotos.setPriority(1);
		
		Thread threadFlickr = new Thread (flickr);
		threadFlickr.setName("Flickr Photos");
		//threadFlickr.setPriority(5);
		
		Thread threadPicassa = new Thread (picassa);
		threadPicassa.setName("Picassa Photos");
		//threadPicassa.setPriority(8);
		
		threadGooglePhotos.start();
		threadFlickr.start();
		threadPicassa.start();
		
		try {
			threadGooglePhotos.join();
			threadFlickr.join();
			threadPicassa.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("=========================================================");
		
		System.out.println("Main thread completed");
		
	}
	
}
	
class GooglePhotos implements Runnable{

	@Override
	public void run() {
		System.out.println("=====================Thread " + Thread.currentThread().getName() + " starts====================");
		
		System.out.println("Name of  the Thread in run method :" + Thread.currentThread().getName());
		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("=====================Thread " + Thread.currentThread().getName() + " terminates====================");
		
	}
	
}
	
class Flickr implements Runnable{

	@Override
	public void run() {
System.out.println("=====================Thread " + Thread.currentThread().getName() + " starts====================");
		
		System.out.println("Name of  the Thread in run method :" + Thread.currentThread().getName());
		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("=====================Thread " + Thread.currentThread().getName() + " terminates====================");
		
	}
		
}

class Picassa implements Runnable{

	@Override
	public void run() {
System.out.println("=====================Thread " + Thread.currentThread().getName() + " starts====================");
		
		System.out.println("Name of  the Thread in run method :" + Thread.currentThread().getName());
		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("=====================Thread " + Thread.currentThread().getName() + " terminates====================");
		
	}
	
}


