package com.hsbc.da1.threadsAssignment;

public class ThreadQuestion1 extends Thread{

	public static void main(String[] args) {
		
		System.out.println("Thread when the program is starting : " + Thread.currentThread().getName());
		
		System.out.println("=========================================================");
		
		ThreadQuestion1 t = new ThreadQuestion1();
		t.setName("Child thread for the question");
		
		t.start();
		
		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("=========================================================");
		
		System.out.println("Main thread is completed");
	}
	
	
	
	@Override
	public void run() {
		
		System.out.println("=====================Thread " + Thread.currentThread().getName() + " starts====================");
			
		System.out.println("Name of  the Thread in run method :" + Thread.currentThread().getName());
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("=====================Thread " + Thread.currentThread().getName() + " terminates====================");
	}
}


