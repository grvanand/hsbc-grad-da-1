package com.hsbc.da1.concurrency;

public class PrinterDemo {
	
	public static void main(String[] args) {
		
		/*
		Printer printer1 = new Printer();
		Printer printer2 = new Printer();
		Printer printer3 = new Printer();

		Job job1 = new Job(printer1, 10, 15);
		Job job2 = new Job(printer2, 50, 60);
		Job job3 = new Job(printer3, 80, 95);
		*/ 
		//Giving 3 different printers 3 different jobs; each printer has only 1 job, no problem of concurrency
		//no need to use "synchronized" concept of below in this case, as here no resource is shared
		
		Printer printer1 = new Printer();
		
		Job job1 = new Job(printer1, 10, 15);
		Job job2 = new Job(printer1, 50, 60);
		Job job3 = new Job(printer1, 80, 95);
		//Assigning 3 different jobs to a single printer, concurrency problem
		
		Thread task1 = new Thread(job1);
		Thread task2 = new Thread(job2);
		Thread task3 = new Thread(job3);

		task1.start();
		task2.start();
		task3.start();

		try {
			task1.join();
			task2.join();
			task3.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(" Completed all the tasks");
	}

}

class Printer {
	
	//when there is a case of shared resource then "synchronized" play an important role
	
	//public void print(int start, int end) throws InterruptedException { - can be used when no "synchronized" aspect done in run()
	public synchronized void print(int start, int end) throws InterruptedException {
		if (end >= start) {
			System.out.println("Printing the " + start + " page");
			for (int i = start+1; i < end; i++) {
				System.out.println("Printing the " + i + " page");
				Thread.sleep(1000);
			}
			System.out.println("Printing the " + end + " page");
		}
	}//good practice is to use "synchronized" keyword in method only and not in run()
}

class Job implements Runnable {

	private Printer printer;
	int start;
	int end;

	public Job(Printer printer, int start, int end) {
		this.printer = printer;
		this.start = start;
		this.end = end;
	}

	@Override
	public void run() {
		try {
			/*
			synchronized (printer) {
				printer.print(start, end);
			}
			*/ 
			//used when not using "synchronized" keyword in the print method 
			printer.print(start, end);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}