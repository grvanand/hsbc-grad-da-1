package com.hsbc.da1.concurrent;

public class WorkerThread implements Runnable {

	private String command;

	public WorkerThread(String job) {
		this.command = job;
	}

	@Override
	public void run() {
		System.out.println(" Worker Job is started ");
		processJob();
		System.out.println(" Worker Job is completed ");

	}

	private void processJob() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			System.out.println("Job could not be completed");
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return "Command : " + this.command;
	}

}
