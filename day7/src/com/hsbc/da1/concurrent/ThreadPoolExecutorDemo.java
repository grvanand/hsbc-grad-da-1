package com.hsbc.da1.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolExecutorDemo {

	public static void main(String[] args) {

		ExecutorService executorService = Executors.newCachedThreadPool();

		for (int i = 0; i < 5; i++) {
			Runnable job = new WorkerThread("Job-" + i);
			executorService.submit(job);
		}

		executorService.shutdown();

		while (!executorService.isTerminated()) {
			System.out.println(" Processing is still pending ");
		}
		System.out.println(" All the jobs are completed....");
	}

}
