package com.hsbc.da1.latch;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchDemo {

	public static void main(String[] args) throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(10);
		
		Worker job = new Worker(1000, latch, "Job");
		
		Thread task = new Thread(job);
		task.start();
		
		latch.await();
		
		System.out.println("All the tasks are now completed");
	}
	
}
