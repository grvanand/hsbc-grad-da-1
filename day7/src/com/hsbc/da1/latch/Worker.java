package com.hsbc.da1.latch;

import java.util.concurrent.CountDownLatch;

public class Worker implements Runnable {
	
	private String message;
	private CountDownLatch countdownLatch;
	private int delay;

	public Worker(int delay, CountDownLatch latch, String message) {
		this.message = message;
		this.countdownLatch = latch;
		this.delay = delay;
	}

	@Override
	public void run() {
		boolean flag = false;
		while (!flag) {
			try {
				System.out.println(" Came inside the run method..");
				Thread.sleep(delay);
				countdownLatch.countDown();
				System.out.println(" Processed the message " + message + " after " + this.countdownLatch.getCount());
				if (countdownLatch.getCount() == 0) {
					flag = true;
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
