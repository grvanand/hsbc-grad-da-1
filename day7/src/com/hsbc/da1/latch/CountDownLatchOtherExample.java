package com.hsbc.da1.latch;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchOtherExample {
	
	public static void main(String[] args) throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(4);
		
		Worker job1 = new Worker(1000, latch, "Job");
		Worker job2 = new Worker(1000, latch, "Job");
		Worker job3 = new Worker(1000, latch, "Job");
		Worker job4 = new Worker(1000, latch, "Job");
		
		Thread task1 = new Thread(job1);
		Thread task2 = new Thread(job2);
		Thread task3 = new Thread(job3);
		Thread task4 = new Thread(job4);
		
		task1.start();
		task2.start();
		task3.start();
		task4.start();
		
		task1.join();
		task2.join();
		task3.join();
		task4.join();
		
		latch.await();
		
		System.out.println("All the tasks are now completed");
	}

}
