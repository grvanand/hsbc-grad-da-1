package com.hsbc.da1.inventory;

import java.util.ArrayList;
import java.util.List;

public class InventoryDemo {

	private static List<Integer> inventory = new ArrayList<>();
	private static int MAX_SIZE = 10;

	public static void main(String[] args) {

		Producer producer = new Producer(inventory, MAX_SIZE);
		Consumer consumer = new Consumer(inventory);

		Thread producerThread = new Thread(producer);
		Thread consumerThread = new Thread(consumer);

		producerThread.start();
		consumerThread.start();

		try {
			producerThread.join();
			consumerThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}

class Producer implements Runnable {
	
	private List<Integer> inventory;
	private int maxSize;
	
	private static int counter;
	
	public Producer(List<Integer> inventory, int maxSize) {
		this.inventory = inventory;
		this.maxSize = maxSize;
	}

	@Override
	public void run() {
		try {
			produce();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
	}
	
	private synchronized void produce() throws InterruptedException {
		while(true) {
			System.out.println("Size " + this.inventory.size());
			if(this.inventory.size() == this.maxSize) {
				System.out.println("Inventory is full. Stop Producing.");
				wait();
			} else {
				this.inventory.add(counter);
				notifyAll();
				Thread.sleep(2000);
			}
		}
		
	}
	
}

class Consumer implements Runnable {
	
	private List<Integer> inventory;
		
	private static int counter = 0;
	
	public Consumer(List<Integer> inventory) {
		this.inventory = inventory;
	}

	@Override
	public void run() {
		try {
			consume();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
	}
	
	private synchronized void consume() throws InterruptedException {
		while(true) {
			if(this.inventory.isEmpty()) {
				System.out.println("Inventory is empty. Initiate Producing message to producer.");
				wait();
			} else {
				System.out.println("Cunsuming the elements from the inventory");
				System.out.println("Item " + inventory.remove(counter));
				notifyAll();
				Thread.sleep(2000);
			}
		}
		
	}
	
}
