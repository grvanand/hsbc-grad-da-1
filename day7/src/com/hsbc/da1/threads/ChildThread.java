package com.hsbc.da1.threads;

public class ChildThread extends Thread {

	public static void main(String[] args) {
		
		System.out.println("Main thread when the program is starting : " + Thread.currentThread().getName());
		
		ChildThread t1 = new ChildThread();
		t1.setName("thread 1");
		
		ChildThread t2 = new ChildThread();
		t2.setName("thread 2");
		
		t1.start();
		t2.start();
		
		for(int i = 0; i<5; i++) {
			System.out.println("Inside Main : " + Thread.currentThread().getName());
			try {
				Thread.currentThread().sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Main thread is completed");
		
	}
	
	
	@Override
	public void run() {
		System.out.println("=====================Thread " + Thread.currentThread().getName() + " starts====================");
		
		for(int i =0; i<10; i++) {
			
			System.out.println("Name of  the Thread :" + Thread.currentThread().getName());
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("=====================Thread " + Thread.currentThread().getName() + " terminates====================");
	}
}
