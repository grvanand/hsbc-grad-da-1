package com.hsbc.da1.threads;

public class ThreadDemo {
	
	public static void main(String[] args) {
		
		Thread t = Thread.currentThread();
		System.out.println("Name of the thread : " + t.getName());
		
		for(int i = 0; i < 10; i++) {
			System.out.println("The name of the thread id : " + t.getName());
			System.out.println("The state of thread id : " + t.getState().name());
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				System.out.println("Thread interrupted while in sleep");
			} finally {
				System.out.println("The state of the Thread id in the finally block : " + t.getState().name());
			}
		}
	}

}
