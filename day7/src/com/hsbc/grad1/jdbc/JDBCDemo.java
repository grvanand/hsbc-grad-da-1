package com.hsbc.grad1.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCDemo {
	
	public static void main(String[] args) {
		
		try(Connection conn = DriverManager.getConnection("jdbc:derby://localhost:1527/mydb","admin","password"); 
				Statement smt = conn.createStatement();)
		{
			int count = smt.executeUpdate("insert into items (name, price) values ('cellphone', 38000)");
			count = smt.executeUpdate("insert into items (name, price) values ('Tablet', 78000)");
			System.out.println(count);
			
			ResultSet result = smt.executeQuery("SELECT * FROM ITEMS");
			
			while(result.next()) {
				System.out.println("Item Name : " + result.getString(1) + " Price : " + result.getDouble(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
