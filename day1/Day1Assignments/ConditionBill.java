public class ConditionBill{

    public static void main(String args[]){

        double bill = Double.parseDouble(args[0]);
        args[1] = args[1].toUpperCase();
    
        if(args[1].equals("KA")){
            System.out.println("Total Bill Amount = " + ((0.15*bill)+bill));
        }
        else if(args[1].equals("TN")){
            System.out.println("Total Bill Amount = " + ((0.18*bill)+bill));
        }
        else if(args[1].equals("MH")){
            System.out.println("Total Bill Amount = " + ((0.20*bill)+bill));
        }
        else{
            System.out.println("Total Bill Amount = " + ((0.12*bill)+bill));
        }
        
    }
}
