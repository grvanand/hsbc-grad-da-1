public class Vowel{

    public static void main(String args[]){

        args[0] = args[0].toLowerCase();

        if(args[0] == "a" | args[0] == "e" | args[0] == "i" | args[0] == "o" | args[0] == "u"){
            System.out.println(args[0] + " is a Vowel.");
        }
        else if(args[0].charAt(0) >= 'a' & args[0].charAt(0) <= 'z'){
            System.out.println(args[0] + " is a Consonant.");
        }
        else{
            System.out.println(args[0] + " is neither a Vowel nor a Consonant.");
        }
    }
}
