public class SavingsAccount{

    public static long accountNumberTracker = 1000;

    private long accountNumber;
    private double accountBalance;
    private String customerName;
    private Address address;
    private String emailAddress;
    private String nominee;

    public SavingsAccount(String customerName, double initialAccountBalance){
        this.accountNumber = ++accountNumberTracker;
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;  
    }

    public SavingsAccount(String customerName, double initialAccountBalance, String nominee){
        this.accountNumber = ++accountNumberTracker;
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.nominee = nominee;
    }

    public SavingsAccount(String customerName, double initialAccountBalance, Address address){
        this.accountNumber = ++accountNumberTracker;
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.address = address;
    }

    public long getAccountNumber(){
        return this.accountNumber;
    }

    public double withdraw(double amount){
       if(this.accountBalance >= amount){
           this.accountBalance = this.accountBalance -amount;
           return amount;
       }
        return 0;
    }

    public double checkBalance(){
        return this.accountBalance;
    }

    public double deposit(double amount){
        this.accountBalance = this.accountBalance + amount;
        return this.accountBalance;
    }

    public void updateAddress(Address address){
        this.address = address;
    }

    public void deposit(double amount, SavingsAccount user){
        user.accountBalance = user.accountBalance + amount;
    }

    public void transferAmount(double amount, long userAccountId) {
        
        if( amount <= this.accountBalance ){
            SavingsAccount userAccount = SavingsAccountRegister.fetchSavingsAccountByAccountId(userAccountId);
            if( userAccount != null){
                System.out.println("Amount deducted : " + withdraw(amount));
                deposit(amount , userAccount);
                System.out.println("Transfer successful!");
            }
            else{
                System.out.println("Invalid receipent");
            }
            
        } else{
            System.out.println("Insufficient Account Balance");
        }
        return;
        
    }
    
}