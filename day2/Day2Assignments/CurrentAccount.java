public class CurrentAccount{

    //static variables
    public long accountNumberTracker = 10_000;

    //insatance variables 
    private long accountNumber;
    private double accountBalance;
    private String customerName;
    private String street;
    private String city;
    private String zipCode;
    private String businessName;
    private String gstNumber;

    public CurrentAccount(String customerName, double initialAccountBalance, String gstNumber, String businessName){
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.accountNumber = ++accountNumberTracker;
        this.gstNumber = gstNumber;
        this.businessName = businessName;
    }

    public CurrentAccount(String customerName, double initialAccountBalance, String gstNumber, String businessName, String street, String city, String zipCode){
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.accountNumber = ++accountNumberTracker;
        this.gstNumber = gstNumber;
        this.businessName = businessName;
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
    }

    public CurrentAccount(String customerName, String gstNumber, String businessName, String street, String city, String zipCode){
        this.customerName = customerName;
        this.accountBalance = 50_000;
        this.accountNumber = ++accountNumberTracker;
        this.gstNumber = gstNumber;
        this.businessName = businessName;
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
    }

    public CurrentAccount(String customerName, String gstNumber, String businessName){
        this.customerName = customerName;
        this.accountBalance = 50_000;
        this.accountNumber = ++accountNumberTracker;
        this.gstNumber = gstNumber;
        this.businessName = businessName;
    }


    //instance methods
    public long getAccountNumber(){
        return this.accountNumber;
    }

    public double withdraw(double amount){
       if((this.accountBalance - 50_000) >= amount){
           this.accountBalance = this.accountBalance -amount;
           return amount;
       }
        return 0;
    }

    public double checkBalance(){
        return this.accountBalance;
    }

    public String checkGstNumber(){
        return this.gstNumber;
    }

    public String businessName(){
        return this.businessName;
    }

    public double deposit(double amount){
        this.accountBalance = this.accountBalance + amount;
        return this.accountBalance;
    }

    public void updateAddress(String street , String city , String zipCode){
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
    }

    
}