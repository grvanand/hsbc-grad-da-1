public class CurrentAccountAddressClient{
    
    public static void main(String[] args){
        
        CurrentAccount raj = new CurrentAccount("Raj" , 1_00_000, "NH10234", "Exports");

        System.out.println("Account Number "+ raj.getAccountNumber());
        System.out.println("GST Number "+ raj.checkGstNumber());
        System.out.println("Business Name "+ raj.businessName());
        System.out.println("Initial Account Balance "+ raj.checkBalance());
        System.out.println("Account Balance "+ raj.deposit(4000));
        System.out.println("Balance after deposit "+ raj.checkBalance());
        raj.withdraw(2000);
        System.out.println("Balance after withdrawl "+ raj.checkBalance());

        System.out.println("-----------------------------------------------------");

        CurrentAccount raju = new CurrentAccount("Raju" , 80_000, "NH10238", "Sales", "Lake Road", "Jaipur", "749694");

        System.out.println("Account Number "+ raju.getAccountNumber());
        System.out.println("GST Number "+ raju.checkGstNumber());
        System.out.println("Business Name "+ raju.businessName());
        System.out.println("Initial Account Balance "+ raju.checkBalance());
        System.out.println("Account Balance "+ raju.deposit(4000));
        System.out.println("Balance after deposit "+ raju.checkBalance());
        raju.withdraw(2000);
        System.out.println("Balance after withdrawl "+ raju.checkBalance());

        System.out.println("-----------------------------------------------------");

    }
}