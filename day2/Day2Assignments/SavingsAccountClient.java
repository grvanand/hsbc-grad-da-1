public class SavingsAccountClient{

    public static void main(String[] args){
        
        SavingsAccount raj = new SavingsAccount("Raj" , 10000);

        System.out.println("Account Number "+ raj.getAccountNumber());
        System.out.println("Initial Account Balance "+ raj.checkBalance());
        System.out.println("Account Balance "+ raj.deposit(4000));
        System.out.println("Balance after deposit "+ raj.checkBalance());
        raj.withdraw(2000);
        System.out.println("Balance after withdrawl "+ raj.checkBalance());

        System.out.println("-----------------------------------------------------");

        SavingsAccount raju = new SavingsAccount("Raju" , 5000, "Tina");

        System.out.println("Account Number "+ raju.getAccountNumber());
        System.out.println("Initial Account Balance "+ raju.checkBalance());
        System.out.println("Account Balance "+ raju.deposit(4000));
        System.out.println("Balance after deposit "+ raju.checkBalance());
        raju.withdraw(1000);
        System.out.println("Balance after withdrawl "+ raju.checkBalance());

        System.out.println("-----------------------------------------------------");

        Address address = new Address("8th Ave", "Bangalore", "Karnataka", 577142);
        SavingsAccount pihu = new SavingsAccount("Pihu", 34_000, address);

        pihu.transferAmount(400, 1001);
        System.out.println("-----------------------------------------------------");
        
    }
}