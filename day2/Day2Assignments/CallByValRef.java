public class CallByValRef{

    public static void main(String[] args) {

        int operand1 = 4;
        int operand2 = 5;

        System.out.println("Changes before call by value : \t" + operand1 +"\t"+ operand2);

        callByValue(operand1 , operand2);

        System.out.println("-------------------------------");

        int array[] = new int[] { 1, 2, 3, 4 };

        System.out.println("Changes before call by reference :");
        for (int a : array) {
            System.out.println(a);
        }

        callByRef(array);

    }

    private static void callByValue(int operand1, int operand2) {

        operand1 = operand1 * 2;
        operand2 = operand2 * 2;

        System.out.println("Local changes applied by Call by Value : \t" + operand1 + "\t" + operand2);

    }

    private static void callByRef(int[] arr) {

        System.out.println("Changes inside the method during Call by Reference :");

        arr[0] = 11;
        arr[1] = 22;
        arr[2] = 33;
        arr[3] = 44;
        for (int a : arr) {
            System.out.println(a);
        }

    }
}