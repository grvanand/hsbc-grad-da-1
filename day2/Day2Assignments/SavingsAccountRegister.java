public class SavingsAccountRegister {

    private static SavingsAccount[] savingsAccounts = new SavingsAccount[] {
            new SavingsAccount("Raj" , 10000), new SavingsAccount("Raju" , 10000, "Tina"),
            new SavingsAccount("Pihu", 34_000, new Address("8th Ave", "Bangalore", "Karnataka", 577142))
            
    };

    public static SavingsAccount fetchSavingsAccountByAccountId(long accountId) {

        for (SavingsAccount savingsAccount : savingsAccounts) {
            if (savingsAccount.getAccountNumber() == accountId) {
                return savingsAccount;
            }
        }
        return null;
    }
}