public class SavingsAccount{

    //static variables
    public long accountNumberTracker = 1000;

    //insatance variables - present until the time innstance is alive 
    private long accountNumber;
    private double accountBalance;
    private String customerName;
    private String street;
    private String city;
    private String zipCode;
    private String emailAddress;
    private String nominee;

    /*
    Rules of constructor :
    -Name same a s class name
    -no return type
    -if no parameters passed to constructor, default values assigned
    */
    public SavingsAccount(String customerName, double initialAccountBalance){
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.accountNumber = ++accountNumberTracker;
    }

    public SavingsAccount(String customerName, double initialAccountBalance, String nominee){
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.nominee = nominee;
        this.accountNumber = ++accountNumberTracker;
    }

    public SavingsAccount(String customerName, double initialAccountBalance, String city, String street, String zipCode){
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
        this.accountNumber = ++accountNumberTracker;
    }

    //instance methods
    public long getAccountNumber(){
        return this.accountNumber;
    }

    public double withdraw(double amount){
       if(this.accountBalance >= amount){
           this.accountBalance = this.accountBalance -amount;
           return amount;
       }
        return 0;
    }

    public double checkBalance(){
        return this.accountBalance;
    }

    public double deposit(double amount){
        this.accountBalance = this.accountBalance + amount;
        return this.accountBalance;
    }

    public void updateAddress(String street , String zipcode , String city){
        this.street = street;
        this.zipCode = zipCode;
        this.city = city;
    }

    public static void main(String[] args){
        //data type variable name = new data type
        SavingsAccount raj = new SavingsAccount("Raj" , 10000);

        System.out.println("Account Number "+ raj.getAccountNumber());
        System.out.println("Initial Account Balance "+ raj.checkBalance());
        System.out.println("Account Balance "+ raj.deposit(4000));
        System.out.println("Balance after deposit "+ raj.checkBalance());
        raj.withdraw(2000);
        System.out.println("Balance after withdrawl "+ raj.checkBalance());

        System.out.println("-----------------------------------------------------");

        SavingsAccount raju = new SavingsAccount("Raju" , 10000, "Tina");

        System.out.println("Account Number "+ raju.getAccountNumber());
        System.out.println("Initial Account Balance "+ raju.checkBalance());
        System.out.println("Account Balance "+ raju.deposit(4000));
        System.out.println("Balance after deposit "+ raju.checkBalance());
        raju.withdraw(200);
        System.out.println("Balance after withdrawl "+ raju.checkBalance());

        System.out.println("-----------------------------------------------------");

        SavingsAccount pihu = new SavingsAccount("Pihu", 50000, "8th Ave", "Mysore", "6714890");
        System.out.println("Customer Name " +pihu.customerName);
        

    }
}