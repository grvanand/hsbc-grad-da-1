package com.hsbc.contactApp.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.contactApp.model.User;
import com.hsbc.contactApp.service.ContactAppService;

public class UserRegistration extends HttpServlet {
	
	@Override
	public void doPost (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		String name = req.getParameter("name");
		String phoneNumber = req.getParameter("phone_number");
		
		String input_dob = req.getParameter("dob");
		LocalDate dob = LocalDate.parse(input_dob, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		
		String password = req.getParameter("password");
		
		User user = new User (name, phoneNumber, dob, password);
		
		ContactAppService service = ContactAppService.getContactAppService();
		
		if(service.saveUser(user) != null) {
			res.sendRedirect("RegistrationSuccessfull.jsp");
		} else {
			res.sendRedirect("error.jsp");
		}
	}
}