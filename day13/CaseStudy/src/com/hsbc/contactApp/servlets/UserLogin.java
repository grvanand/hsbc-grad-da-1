package com.hsbc.contactApp.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.contactApp.service.ContactAppService;

public class UserLogin extends HttpServlet {
	
	@Override
	public void doPost (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		String name = req.getParameter("name");
		String password = req.getParameter("password");
		
		ContactAppService service = ContactAppService.getContactAppService();
		
		if(service.loginValidity(name, password)) {
			res.sendRedirect("LoginSuccessfull.jsp");
		} else {
			res.sendRedirect("error.jsp");
		}
	}
}