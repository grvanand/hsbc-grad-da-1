package com.hsbc.contactApp.service;

import java.util.Collection;

import com.hsbc.contactApp.dao.ContactAppDAO;
import com.hsbc.contactApp.exception.UserNotFoundException;
import com.hsbc.contactApp.model.User;

public class ContactAppServiceImpl implements ContactAppService{

	private ContactAppDAO dao = ContactAppDAO.getContactAppDAO();
	
	@Override
	public User saveUser(User localUser) {
		return this.dao.saveUser(localUser);
	}

	@Override
	public User updateUser(long profileId, User localUser) {
		return this.updateUser(profileId, localUser);
	}

	@Override
	public void deleteUser(long profileId) {
		this.dao.deleteUser(profileId);
		
	}

	@Override
	public Collection<User> fetchUsers() {
		return this.dao.fetchUsers();
	}

	@Override
	public User fetchUserByProfileId(long profileId) throws UserNotFoundException {
		return this.dao.fetchUserByProfileId(profileId);
	}

	@Override
	public User fetchUserByName(String name) throws UserNotFoundException {
		return this.dao.fetchUserByName(name);
	}

	@Override
	public boolean loginValidity(String name, String password)  {
		User user = null;
		try {
			user = this.dao.fetchUserByName(name);
		} catch (UserNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		if(password.equalsIgnoreCase(user.getPassword())) {
			return true;
		}
		return false;
	}
}