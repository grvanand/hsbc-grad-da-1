package com.hsbc.contactApp.service;

import java.util.Collection;

import com.hsbc.contactApp.exception.UserNotFoundException;
import com.hsbc.contactApp.model.User;

public interface ContactAppService {

	//static final ContactAppService service = new ContactAppServiceImpl();
	
	static ContactAppService getContactAppService() {
		return new ContactAppServiceImpl();
	}
	
	User saveUser(User localUser);
	
	User updateUser(long profileId, User localUser);
		
	void deleteUser(long profileId);
	
	Collection<User> fetchUsers();
		
	User fetchUserByProfileId(long profileId) throws UserNotFoundException;
	
	User fetchUserByName(String  name) throws UserNotFoundException;
	
	boolean loginValidity(String name, String password);
	
}