package com.hsbc.contactApp.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.hsbc.contactApp.exception.UserNotFoundException;
import com.hsbc.contactApp.model.User;

public class JdbcBackedContactAppDAOImpl implements ContactAppDAO {

	private static String connectString = "jdbc:derby://localhost:1527/contactAppdb";
	private static String username = "admin";
	private static String password = "password";

	
	private static final String INSERT_QUERY_PREP_STMT = "insert into contacts (name, phone_number, dob, password)"
			+ " values  (?, ?, ?, ?)";

	private static final String SELECT_QUERY = "select * from contacts";

	private static final String DELETE_BY_ID_QUERY = "delete  from contacts where profile_id=?";
	
	private static final String SELECT_BY_ID_QUERY = "select *  from contacts where profile_id=?";

	private static final String SELECT_BY_NAME_QUERY = "select * from contacts where name=?";	
	
	private static final String UPDATE_BY_ID_QUERY = "update contacts set name = ?, phone_number = ?, dob = ? where profile_id = ?";
	
	
	private static Connection getDBConnection() {
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver"); 
			Connection connection = DriverManager.getConnection(connectString, username, password);
			return connection;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Override
	public User saveUser(User localUser) {
		int numberOfRecordsUpdated = 0;
		try (PreparedStatement pStmt = getDBConnection().prepareStatement(INSERT_QUERY_PREP_STMT);){
			pStmt.setString(1, localUser.getName());
			pStmt.setString(2, localUser.getPhoneNumber());
			
			Date date = Date.valueOf(localUser.getDob());
			
			pStmt.setDate(3, date);
			pStmt.setString(4, localUser.getPassword());
			numberOfRecordsUpdated = pStmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (numberOfRecordsUpdated == 1) {
			try {
				User fetchedUser = fetchUserByName(localUser.getName());
				if(fetchedUser != null) {
					return fetchedUser;
				}
			} catch (UserNotFoundException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public User fetchUserByName(String name) throws UserNotFoundException {
		try(PreparedStatement ps = getDBConnection().prepareStatement(SELECT_BY_NAME_QUERY);) {       
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {	
				User user = new User(rs.getString("name"), rs.getString("phone_number"), rs.getDate("dob").toLocalDate(), rs.getString("password"));
				//System.out.println("User details successfully added!");
				//System.out.println(user);		
				return user;
			}
		} catch (SQLException e) {
				e.printStackTrace();
				throw new UserNotFoundException("User with name : " + name + " does not exist");
		}
		return null;
	}
	
	@Override
	public User updateUser(long profileId, User localUser) {
		int numberOfRecordsUpdated = 0;
		try(PreparedStatement pStmt = getDBConnection().prepareStatement(UPDATE_BY_ID_QUERY);){
			pStmt.setString(1, localUser.getName());
			pStmt.setString(2, localUser.getPhoneNumber());
			pStmt.setDate(3, Date.valueOf(localUser.getDob()));
			pStmt.setLong(4, profileId);
			numberOfRecordsUpdated = pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			if(numberOfRecordsUpdated == 1) {
				try {
					User update = fetchUserByProfileId(profileId);
					return update;
				} catch (UserNotFoundException e) {
					e.printStackTrace();
				}	
			}
			return null;
		}
		

	@Override
	public void deleteUser(long profileId) {
		int numberOfRecordsDeleted = 0;
		try(PreparedStatement pStmt = getDBConnection().prepareStatement(DELETE_BY_ID_QUERY);){
				pStmt.setLong(1, profileId);
				numberOfRecordsDeleted = pStmt.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		if(numberOfRecordsDeleted == 1) {
			System.out.println("User profile with profile ID : " + profileId + " deleted");
		}
	}
	
	@Override
	public Collection<User> fetchUsers() {
		Set<User> userSet = new HashSet<>();
		try (PreparedStatement pStmt = getDBConnection().prepareStatement(SELECT_QUERY);){
			ResultSet rs = pStmt.executeQuery();
			while(rs.next()) {
				User user = new User(rs.getString("name"), rs.getString("phone_number"), rs.getDate("dob").toLocalDate(), rs.getString("password"));	
				userSet.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userSet;
	}
	
	
	@Override
	public User fetchUserByProfileId(long profileId) throws UserNotFoundException {
		try(PreparedStatement pStmt = getDBConnection().prepareStatement(SELECT_BY_ID_QUERY);){
			pStmt.setLong(1, profileId);
			ResultSet rs = pStmt.executeQuery();
			if(rs.next()) {
				User user = new User(rs.getString("name"), rs.getString("phone_number"), rs.getDate("dob").toLocalDate(), rs.getString("password"));
				return user;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new UserNotFoundException("User with Profile ID : " + profileId + " does not exist");
		}
		return null;
	}
}