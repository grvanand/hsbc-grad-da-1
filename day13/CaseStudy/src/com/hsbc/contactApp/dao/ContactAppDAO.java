package com.hsbc.contactApp.dao;

import java.util.Collection;

import com.hsbc.contactApp.exception.UserNotFoundException;
import com.hsbc.contactApp.model.User;

public interface ContactAppDAO {
	
	//static final ContactAppDAO dao = new JdbcBackedContactAppDAOImpl();
	
	static ContactAppDAO getContactAppDAO() {
		return new JdbcBackedContactAppDAOImpl();
	}
	
	User saveUser(User localUser);
	
	User updateUser(long profileId, User localUser);
		
	void deleteUser(long profileId);
	
	Collection<User> fetchUsers();
		
	User fetchUserByProfileId(long profileId) throws UserNotFoundException;
	
	User fetchUserByName(String  name) throws UserNotFoundException;

}