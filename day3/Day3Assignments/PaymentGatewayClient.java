interface PaymentGateway {
    
    void pay(String from, String to, double amount, String notes);

}


interface MobileRecharge {

    void rechargeMobile(String phonenumber, double amount);

}


class GooglePay implements PaymentGateway, MobileRecharge {

    public void pay(String from, String to, double amount, String notes) {
        System.out.println("Payment from " + from + " to " + to + " of amount " + amount + " notes " + notes + " Using Google Pay.");
    }

    public void rechargeMobile(String phonenumber, double amount) {
        System.out.println("Mobile recharge for " + phonenumber + " with amount  " + amount + " done Using GooglePay.");
    }

}


class PhonePay implements PaymentGateway, MobileRecharge {

    public void pay(String from, String to, double amount, String notes) {
        System.out.println("Payment from " + from + " to " + to + " of amount " + amount + " notes " + notes + " Using Phone Pay.");
    }

    public void rechargeMobile(String phonenumber, double amount) {
        System.out.println("Mobile recharge for " + phonenumber + " with amount  " + amount + " done Using PhonePay.");
    }
}


class JioPay implements PaymentGateway {

    public void pay(String from, String to, double amount, String notes) {
        System.out.println("Payment from " + from + " to " + to + "of amount " + amount + " notes " + notes + " Using Jio Pay.");
    }
}


public class PaymentGatewayClient {

    public static void main(String[] args) {

        PaymentGateway paymentGateway = null;
        MobileRecharge mobileRecharge = null;

        if (args[0].equals("1")) {
            GooglePay googlePay = new GooglePay();
            paymentGateway = googlePay;
            mobileRecharge = googlePay;
        } else if (args[0].equals("2")) {
            PhonePay phonePay = new PhonePay();
            paymentGateway = phonePay;
            mobileRecharge = phonePay;
        } else {
            JioPay jioPay = new JioPay();
            paymentGateway = jioPay;
        }

        paymentGateway.pay("John", "Jack", 5_000, "Please confirm");
        
        if(mobileRecharge != null){
            mobileRecharge.rechargeMobile("99****1234", 100);
        }
        
    }
}