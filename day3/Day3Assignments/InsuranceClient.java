interface Insurance {
    
    double calculatePremium (String vechileName, double insuredAmount, String model);
    String payPremium (double premiumAmount, String vechileNumber, String model);
}


class TataAIG implements Insurance {

    public static long counterTataAIG = 1;

    public double calculatePremium (String vechileName, double insuredAmount, String model){
        if(insuredAmount >= 7_00_000){
            return 0.10*insuredAmount;
        } else if(insuredAmount >= 3_00_000){
            return 0.6*insuredAmount;
        } else if(insuredAmount >= 1_00_000){
            return 0.3*insuredAmount;
        } else {
            return 0.1*insuredAmount;
        }
    }

    public String payPremium (double premiumAmount, String vechileNumber, String model){
        return ("TataAIG" + model + vechileNumber + (++counterTataAIG));
    }

}

class Bajaj implements Insurance {

    public static long counterBajaj = 1;

    public double calculatePremium (String vechileName, double insuredAmount, String model){
        if(insuredAmount >= 10_00_000){
            return 0.12*insuredAmount;
        } else if(insuredAmount >= 8_00_000){
            return 0.10*insuredAmount;
        } else if(insuredAmount >= 2_50_000){
            return 0.6*insuredAmount;
        } else {
            return 0.3*insuredAmount;
        }
    }

    public String payPremium (double premiumAmount, String vechileNumber, String model){
        return ("Bajaj" + model + vechileNumber + (++counterBajaj));
    }

}

public class InsuranceClient {

    public static void main(String[] args) {

        Insurance insurance = null;

        if (args[0].equals("1")) {
            TataAIG tataAIG = new TataAIG();
            insurance = tataAIG;
        } 
        else if (args[0].equals("2")) {
            Bajaj bajaj = new Bajaj();
            insurance = bajaj;
        } 

        double premiumAmount = insurance.calculatePremium("Mazda" , 5_00_000.0, "Sedan");
        
        System.out.println("Premium for your car insurance : " + premiumAmount);

        Savings_Account savingsAccount = new Savings_Account("Rahul" , 10_00_000);

        if (savingsAccount.checkBalance() >= premiumAmount){
            savingsAccount.withdraw(premiumAmount);
            System.out.println("Amount deducted from bank account");

            String policyNumber = insurance.payPremium(premiumAmount , "DL-01H-3456", "Sedan");
            System.out.println("Policy Number for your car insurance : " + policyNumber);
        } else {
            System.out.println("Not Enough Account Balance.");
        }

        

    }
}