final class CurrentAccountAddress extends BankAccount{

    private Address address;
    private String businessName;
    private String gstNumber;
    private long LOAN_ELIGIBILITY = 25_00_000;


    public CurrentAccountAddress(String customerName, double accountBalance){
        super(customerName , accountBalance);
    }

    public CurrentAccountAddress(String customerName, double accountBalance, String gstNumber, String businessName){
        super(customerName , accountBalance);
        this.gstNumber = gstNumber;
        this.businessName = businessName;
    }

    public CurrentAccountAddress(String customerName, double accountBalance, String gstNumber, String businessName, Address address){
        super(customerName , accountBalance);
        this.gstNumber = gstNumber;
        this.businessName = businessName;
        this.address = address;
    }

    public final double withdraw(double amount){
       if((this.accountBalance - 25_000) >= amount){
           this.accountBalance = this.accountBalance -amount;
           return amount;
       }
        return 0;
    }


    public final String checkGstNumber(){
        return this.gstNumber;
    }

    public final String businessName(){
        return this.businessName;
    }
    
    public final double loanEligibility() {
        return LOAN_ELIGIBILITY;
    }
    
    public final double getLoanAmount(double loanAmount) {
        if( loanAmount <= LOAN_ELIGIBILITY){
            return super.deposit(loanAmount);
        } else {
            return 0;
        }
        
    }
    
}