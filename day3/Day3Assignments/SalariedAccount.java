final class SalariedAccount extends BankAccount{

    private Address address;
    private String emailAddress;
    private String nominee;
    private long LOAN_ELIGIBILITY = 10_00_000;
    

    public SalariedAccount(String customerName, double accountBalance){
        super(customerName , accountBalance);
    }

    public SalariedAccount(String customerName, double accountBalance, String nominee){
        super(customerName , accountBalance);
        this.nominee = nominee;
    }

    public SalariedAccount(String customerName, double accountBalance, Address address){
        super(customerName , accountBalance);   
        this.address = address;
    }

    public final double withdraw(double amount){
       if((super.accountBalance  >= amount) & (amount <= 15_000)){
           super.accountBalance = super.accountBalance -amount;
           return amount;
       }
        return 0;
    }
   
    public final double getLoanAmount(double loanAmount) {
        if( loanAmount <= LOAN_ELIGIBILITY){
            return super.deposit(loanAmount);
        } else {
            return 0;
        }
        
    }
    
    public final double loanEligibility() {
        return LOAN_ELIGIBILITY;
    }
    
}