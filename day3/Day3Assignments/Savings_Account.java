public class Savings_Account{

    public long accountNumberTracker = 1000;

    private long accountNumber;
    private double accountBalance;
    private String customerName;
    private String street;
    private String city;
    private String zipCode;
    private String emailAddress;
    private String nominee;

    
    public Savings_Account(String customerName, double initialAccountBalance){
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.accountNumber = ++accountNumberTracker;
    }

    public Savings_Account(String customerName, double initialAccountBalance, String nominee){
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.nominee = nominee;
        this.accountNumber = ++accountNumberTracker;
    }

    public Savings_Account(String customerName, double initialAccountBalance, String city, String street, String zipCode){
        this.customerName = customerName;
        this.accountBalance = initialAccountBalance;
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
        this.accountNumber = ++accountNumberTracker;
    }

    
    public long getAccountNumber(){
        return this.accountNumber;
    }

    public double withdraw(double amount){
       if(this.accountBalance >= amount){
           this.accountBalance = this.accountBalance -amount;
           return amount;
       }
        return 0;
    }

    public double checkBalance(){
        return this.accountBalance;
    }

    public double deposit(double amount){
        this.accountBalance = this.accountBalance + amount;
        return this.accountBalance;
    }

    public void updateAddress(String street , String zipcode , String city){
        this.street = street;
        this.zipCode = zipCode;
        this.city = city;
    }

}