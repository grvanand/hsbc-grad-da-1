abstract class BankAccount{

    public String customerName;
    public double accountBalance;
    public final long accountNumber;
    public Address address;

    public static long counter = 1000;

    public BankAccount(String customerName, double accountBalance){
        this.customerName = customerName;
        this.accountBalance = accountBalance;
        this.accountNumber = ++counter;
    }

    public abstract double getLoanAmount(double loanAmountif);
    public abstract double loanEligibility();
    public abstract double withdraw(double amount);

    public double deposit(double amount){
        this.accountBalance = this.accountBalance + amount;
        return this.accountBalance;
    }

    public long getAccountNumber(){
        return this.accountNumber;
    }

    public double checkBalance(){
        return this.accountBalance;
    }

    public void updateAddress(Address address){
        this.address = address;
    }

    public  final String getCustomerDetails(){
        return "Customer name: " + this.customerName + " Balance: " + this.accountBalance + " A/C No: " + this.accountNumber;
    }

}


public class BankAccountClient{
    
    public static void main(String []args){

        BankAccount bank = null;
        if (args[0].equals("1")){
            bank = new CurrentAccountAddress("Himani" , 50_000);
        } else if (args[0].equals("2")) {
            bank = new SavingsAccount("Himanshu" , 60_000);
        } else if (args[0].equals("3")) {
            bank = new SalariedAccount("Himadri" , 70_000);
        } else {
            System.out.println("Wrong input! Try again...");
            return;
        }
        execute(bank);

    }
        
    public static void execute(BankAccount bank){

        System.out.println("LOAN eligibility of  : " + bank.loanEligibility());
        if(bank.getLoanAmount(2_00_000) != 0){
            System.out.println("Loan Granted.");
        }
        System.out.println(bank.getCustomerDetails());
    }
    
}