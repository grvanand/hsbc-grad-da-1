final class SavingsAccount extends BankAccount{

    private Address address;
    private String emailAddress;
    private String nominee;
    private long LOAN_ELIGIBILITY = 5_00_000;


    public SavingsAccount(String customerName, double accountBalance){
        super(customerName , accountBalance); 
    }

    public SavingsAccount(String customerName, double accountBalance, String nominee){
        super(customerName , accountBalance);
        this.nominee = nominee;
    }

    public SavingsAccount(String customerName, double accountBalance, Address address){
        super(customerName , accountBalance);
        this.address = address;
    }

    public final double withdraw(double amount){
       if((this.accountBalance - 10_000 >= amount) & (amount <= 10_000)){
           this.accountBalance = this.accountBalance -amount;
           return amount;
       }
        return 0;
    }
    
    public final double loanEligibility() {
        return LOAN_ELIGIBILITY;
    }
    

    public final double getLoanAmount(double loanAmount) {
        if( loanAmount <= LOAN_ELIGIBILITY){
            return super.deposit(loanAmount);
        } else {
            return 0;
        }
        
    }

        
    
}