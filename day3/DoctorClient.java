abstract class Doctor{ //if any method is abstract in the class, the class has to be abstract

    private String name;
    private String specialization;
    private int age;

    public abstract void treatPatients();
    /*{
        System.out.println("Treating Patients");
    }*/
}

class Orthopedecian extends Doctor{
    
    public void treatPatients(){
        conductCTScan();
        conductXRay();
    }

    //specialization methods
    public void conductCTScan(){
        System.out.println("Conducting CT Scan");

    }

    public void conductXRay(){
        System.out.println("Conducting X Ray");
    }
}

class Padietrician extends Doctor{

    public void treatPatients(){
        treatKids();
    }

    public void treatKids(){
        System.out.println("Treating Kids");
    }
}

class Dentist extends Doctor{

    public void treatPatients(){
        rootCanal();
    }

    public void rootCanal(){
        System.out.println("Performing Root Canal operation");
    }
}


public class DoctorClient{
    
    public static void main(String []args){

        //Doctor doc = new Doctor();
       // doc.treatPatients();

        Orthopedecian ortho = new Orthopedecian();
        /*
        ortho.treatPatients();
        ortho.conductCTScan();
        ortho.conductXRay();
        */

       // Doctor orthopedic = new Orthopedecian();//upcasting


        Doctor doc = null;
        String typeOfDoc = agrs[0];
        if (typeOfDoc.equals("Ortho")){
            doc = new Orthopedecian();
        } else if (typeOfDoc == "padietric") {
            doc = new Padietrician();
        } else {
            doc = new Dentist();
        }
        execute(orthopedic);

    }
        

    public static void execute(Doctor doc){

        /*if(doc instanceof Orthopedecian){    
        Orthopedecian orthopedician = (Orthopedecian) doc;
        orthopedician.treatPatients();
        orthopedician.conductCTScan();
        orthopedician.conductXRay();
        }*/
        doc.treatPatients();
    }
    
}