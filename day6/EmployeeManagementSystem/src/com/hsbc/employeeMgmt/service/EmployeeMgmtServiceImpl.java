package com.hsbc.employeeMgmt.service;

import java.util.Set;

import com.hsbc.employeeMgmt.dao.EmployeeMgmtDAO;
import com.hsbc.employeeMgmt.exception.EmployeeNotFoundException;
import com.hsbc.employeeMgmt.exception.InsufficientLeaveException;
import com.hsbc.employeeMgmt.model.EmployeeMgmt;

public class EmployeeMgmtServiceImpl implements EmployeeMgmtService {
	

	private EmployeeMgmtDAO dao ;
	
	public EmployeeMgmtServiceImpl(EmployeeMgmtDAO dao) {
		this.dao = dao;
	}
	
	@Override
	public EmployeeMgmt createEmployee(String employeeName, double salary, int age) {
	
		EmployeeMgmt employee = new EmployeeMgmt(employeeName, salary, age);
		
		EmployeeMgmt employeeCreated = this.dao.saveEmployee(employee);
		return employeeCreated;
	}
	
	@Override
	public EmployeeMgmt createEmployee(String employeeName, double salary) {
		EmployeeMgmt employee = new EmployeeMgmt(employeeName, salary);
		
		EmployeeMgmt employeeCreated = this.dao.saveEmployee(employee);
		return employeeCreated;
	}
	
	@Override
	public void deleteEmployee(long employeeId) {
		this.dao.deleteEmployee(employeeId);
	}
	
	@Override
	public EmployeeMgmt fetchEmployeeByEmployeeId(long employeeId) throws EmployeeNotFoundException {
		EmployeeMgmt employee = this.dao.fetchEmployeeByEmployeeId(employeeId);
		return employee;
	}
	
	@Override
	public Set<EmployeeMgmt> fetchEmployees() {
		return this.dao.fetchEmployees();
		
	}
	
	@Override
	public EmployeeMgmt fetchEmployeeByEmployeeName(String employeeName) throws EmployeeNotFoundException {
		EmployeeMgmt employee = this.dao.fetchEmployeeByEmployeeName(employeeName);
		return employee;
	}

	@Override
	public int leaveRequest(long employeeId, int leave) throws EmployeeNotFoundException, InsufficientLeaveException {
		EmployeeMgmt employee = this.dao.fetchEmployeeByEmployeeId(employeeId);
		if(employee != null ) {
			if(leave > employee.getLeave()) {
				throw new InsufficientLeaveException("Insufficient leaves");
			}
			if(leave > 10) {
				return 0;
			} else {
				employee.setLeave(employee.getLeave() - leave);
				this.dao.updateEmployee(employeeId, employee);
				return 1;
			}
		}
		return 0;
		
	}

}
