package com.hsbc.employeeMgmt.service;

import com.hsbc.employeeMgmt.exception.EmployeeNotFoundException;
import com.hsbc.employeeMgmt.exception.InsufficientLeaveException;
import com.hsbc.employeeMgmt.model.EmployeeMgmt;

import java.util.*;

public interface EmployeeMgmtService {
	
	EmployeeMgmt createEmployee(String employeeName, double salary, int age);
	
	EmployeeMgmt createEmployee(String employeeName, double salary);

	void deleteEmployee(long employeeId);
	
	EmployeeMgmt fetchEmployeeByEmployeeId(long employeeId) throws EmployeeNotFoundException;
		
	Set<EmployeeMgmt> fetchEmployees();
	
	EmployeeMgmt fetchEmployeeByEmployeeName(String employeeName) throws EmployeeNotFoundException;
	
	int leaveRequest(long employeeId, int leave) throws EmployeeNotFoundException, InsufficientLeaveException;
	
}
