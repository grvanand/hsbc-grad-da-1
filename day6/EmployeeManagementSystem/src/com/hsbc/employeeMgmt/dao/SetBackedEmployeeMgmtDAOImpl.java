package com.hsbc.employeeMgmt.dao;

import java.util.*;

import com.hsbc.employeeMgmt.exception.EmployeeNotFoundException;
import com.hsbc.employeeMgmt.model.EmployeeMgmt;

public class SetBackedEmployeeMgmtDAOImpl implements EmployeeMgmtDAO {
	
	private Set<EmployeeMgmt> employeeSet = new HashSet<>();
	
	@Override
	public EmployeeMgmt saveEmployee(EmployeeMgmt employee){
		this.employeeSet.add(employee);
		return employee;		
	}
	
	@Override
	public EmployeeMgmt updateEmployee(long employeeId, EmployeeMgmt localemployee) {
		for(EmployeeMgmt employee : employeeSet) {
			if(employee != null && employee.getEmployeeId() == employeeId) {
				employee = localemployee;
			}
		}
		return localemployee;
	}
	
	@Override
	public void deleteEmployee(long employeeId) {
		for(EmployeeMgmt employee : employeeSet) {
			if(employee != null && employee.getEmployeeId() == employeeId) {
				this.employeeSet.remove(employee);
			}
		}
	}
	
	@Override
	public Set<EmployeeMgmt> fetchEmployees() {
		return (Set<EmployeeMgmt>) this.employeeSet;
	}
	
	@Override
	public EmployeeMgmt fetchEmployeeByEmployeeId(long employeeId) throws EmployeeNotFoundException {
		for(EmployeeMgmt employee : employeeSet) {
			if(employee != null && employee.getEmployeeId() == employeeId) {
				return employee;
			}
		}
		throw new EmployeeNotFoundException("No Employee found");
	}

	@Override
	public EmployeeMgmt fetchEmployeeByEmployeeName(String employeeName) throws EmployeeNotFoundException {
		for(EmployeeMgmt employee : employeeSet) {
			if(employee != null && employee.getEmployeeName().equals(employeeName)){
				return employee;
			}
		}
		throw new EmployeeNotFoundException("No Employee found");
	}

}
