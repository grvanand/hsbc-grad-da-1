package com.hsbc.employeeMgmt.dao;

import java.util.*;

import com.hsbc.employeeMgmt.exception.EmployeeNotFoundException;
import com.hsbc.employeeMgmt.model.EmployeeMgmt;

public class ListBackedEmployeeMgmtDAOImpl implements EmployeeMgmtDAO{
	
	private List<EmployeeMgmt> employeeList = new ArrayList<>();
	
	public EmployeeMgmt saveEmployee(EmployeeMgmt employee){
		this.employeeList.add(employee);
		return employee;		
	}
	
	public EmployeeMgmt updateEmployee(long employeeId, EmployeeMgmt localemployee) {
		for(EmployeeMgmt employee : employeeList) {
			if(employee != null && employee.getEmployeeId() == employeeId) {
				employee = localemployee;
			}
		}
		return localemployee;
	}
	
	public void deleteEmployee(long employeeId) {
		for(EmployeeMgmt employee : employeeList) {
			if(employee != null && employee.getEmployeeId() == employeeId) {
				this.employeeList.remove(employee);
			}
		}
	}
	
	public Set<EmployeeMgmt> fetchEmployees() {
		return (Set<EmployeeMgmt>) this.employeeList;
	}
	
	public EmployeeMgmt fetchEmployeeByEmployeeId(long employeeId) throws EmployeeNotFoundException {
		for(EmployeeMgmt employee : employeeList) {
			if(employee != null && employee.getEmployeeId() == employeeId) {
				return employee;
			}
		}
		throw new EmployeeNotFoundException("No Employee found");
	}

	@Override
	public EmployeeMgmt fetchEmployeeByEmployeeName(String employeeName) throws EmployeeNotFoundException {
		for(EmployeeMgmt employee : employeeList) {
			if(employee != null && employee.getEmployeeName().equals(employeeName)){
				return employee;
			}
		}
		throw new EmployeeNotFoundException("No Employee found");
	}

}
