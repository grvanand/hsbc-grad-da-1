package com.hsbc.employeeMgmt.dao;

import com.hsbc.employeeMgmt.exception.EmployeeNotFoundException;
import com.hsbc.employeeMgmt.model.EmployeeMgmt;

import java.util.*;

public interface EmployeeMgmtDAO {
	
	EmployeeMgmt saveEmployee(EmployeeMgmt employee);
	
	EmployeeMgmt updateEmployee(long employeeId, EmployeeMgmt employee);
		
	void deleteEmployee(long employeeId);
	
	Set<EmployeeMgmt> fetchEmployees();
		
	EmployeeMgmt fetchEmployeeByEmployeeId(long employeeId)throws EmployeeNotFoundException;
	
	EmployeeMgmt fetchEmployeeByEmployeeName(String employeeName)throws EmployeeNotFoundException;
		

}
