package com.hsbc.employeeMgmt.util;


import com.hsbc.employeeMgmt.dao.EmployeeMgmtDAO;
import com.hsbc.employeeMgmt.dao.ListBackedEmployeeMgmtDAOImpl;
import com.hsbc.employeeMgmt.dao.SetBackedEmployeeMgmtDAOImpl;

public class EmployeeMgmtDAOFactory {
	
	public static EmployeeMgmtDAO getEmployeeMgmtDAO() {
			return new SetBackedEmployeeMgmtDAOImpl();
	}
			
}
