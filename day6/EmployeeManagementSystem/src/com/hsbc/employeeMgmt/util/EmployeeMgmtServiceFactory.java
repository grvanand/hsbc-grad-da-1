package com.hsbc.employeeMgmt.util;

import com.hsbc.employeeMgmt.dao.EmployeeMgmtDAO;
import com.hsbc.employeeMgmt.service.EmployeeMgmtService;
import com.hsbc.employeeMgmt.service.EmployeeMgmtServiceImpl;

public class EmployeeMgmtServiceFactory {

	public static EmployeeMgmtService getInstance(EmployeeMgmtDAO dao) {
		return new EmployeeMgmtServiceImpl(dao);
	}
}
