package com.hsbc.employeeMgmt.exception;

public class InsufficientLeaveException extends Exception {
	
	public InsufficientLeaveException(String  message) {
		super(message);
	}
	
	@Override
	public String getMessage() {
		return super.getMessage();
	}

}
