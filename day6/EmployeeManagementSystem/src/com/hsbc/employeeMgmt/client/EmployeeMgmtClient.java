package com.hsbc.employeeMgmt.client;

import java.util.*;

import com.hsbc.employeeMgmt.controller.EmployeeMgmtController;
import com.hsbc.employeeMgmt.dao.EmployeeMgmtDAO;
import com.hsbc.employeeMgmt.exception.EmployeeNotFoundException;
import com.hsbc.employeeMgmt.exception.InsufficientLeaveException;
import com.hsbc.employeeMgmt.model.*;
import com.hsbc.employeeMgmt.service.EmployeeMgmtService;
import com.hsbc.employeeMgmt.util.*;

public class EmployeeMgmtClient {
	
	public static void main(String[] args) throws InsufficientLeaveException, EmployeeNotFoundException {
		
		EmployeeMgmtDAO dao = EmployeeMgmtDAOFactory.getEmployeeMgmtDAO();
		
		EmployeeMgmtService service = EmployeeMgmtServiceFactory.getInstance(dao);
		
		EmployeeMgmtController controller = new EmployeeMgmtController(service);
		
		EmployeeMgmt user1 = controller.openEmployee("Rohan", 25_000, 45);
		EmployeeMgmt user2 = controller.openEmployee("Joshi", 50_000);
		
		try {
			int request;
			request = controller.leaveRequest(user1.getEmployeeId(), 5);
			if(request == 1) {
				System.out.println("Leave Granted.");
			} else {
				System.out.println("Leave cannot be granted.");
			}
			
		} catch (InsufficientLeaveException | EmployeeNotFoundException exception) {
			System.out.println(exception.getMessage());	
		}
		
		System.out.println("====================================================");
		
		Set<EmployeeMgmt> employeeSet = controller.fetchEmployees();
		
		for(EmployeeMgmt employee: employeeSet) {
			if ( employee != null) {
				System.out.println("Employee Id : "+ employee.getEmployeeId());
				System.out.println("Employee Name : "+ employee.getEmployeeName());
			}
		}
		
		System.out.println("====================================================");
		
		for(EmployeeMgmt employee: employeeSet) {
			if ( employee != null) {
				System.out.println("Employee : " + employee);
			}
		}
		
		System.out.println("====================================================");
	}
}
