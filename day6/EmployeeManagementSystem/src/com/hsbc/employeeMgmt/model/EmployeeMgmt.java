package com.hsbc.employeeMgmt.model;

public class EmployeeMgmt {
	
	private String employeeName;
	private long employeeId;
	private double salary;
	private int age;
	private int leave;
	
	private static long counter = 1000;
	
	public EmployeeMgmt(String employeeName, double salary, int age) {
		this.employeeName = employeeName;
		this.salary = salary;
		this.age = age;
		this.leave = 40;
		this.employeeId = ++counter;
		
	}
	
	public EmployeeMgmt(String employeeName, double salary) {
		this.employeeName = employeeName;
		this.salary = salary;
		this.leave = 40;
		this.employeeId = ++counter;
		
	}

	/**
	 * @return the employeeName
	 */
	public String getEmployeeName() {
		return employeeName;
	}

	/**
	 * @param employeeName the employeeName to set
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	/**
	 * @return the employeeId
	 */
	public long getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the salary
	 */
	public double getSalary() {
		return salary;
	}

	/**
	 * @param salary the salary to set
	 */
	public void setSalary(double salary) {
		this.salary = salary;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * @return the leave
	 */
	public int getLeave() {
		return leave;
	}

	/**
	 * @param leave the leave to set
	 */
	public void setLeave(int leave) {
		this.leave = leave;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (employeeId ^ (employeeId >>> 32));
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof EmployeeMgmt))
			return false;
		EmployeeMgmt other = (EmployeeMgmt) obj;
		if (employeeId != other.employeeId)
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EmployeeMgmt [employeeName=" + employeeName + ", employeeId=" + employeeId + ", salary=" + salary
				+ ", age=" + age + ", leave=" + leave + "]";
	}

	

}
