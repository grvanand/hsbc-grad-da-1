package com.hsbc.employeeMgmt.controller;

import com.hsbc.employeeMgmt.exception.EmployeeNotFoundException;
import com.hsbc.employeeMgmt.exception.InsufficientLeaveException;
import com.hsbc.employeeMgmt.model.EmployeeMgmt;
import com.hsbc.employeeMgmt.service.*;

import java.util.*;

public class EmployeeMgmtController {
	

	private EmployeeMgmtService service;
	
	public EmployeeMgmtController(EmployeeMgmtService service) {
		this.service = service;
	}

	public EmployeeMgmt openEmployee(String employeeName, double salary, int age) {
		
		return this.service.createEmployee(employeeName, salary, age);
	}
	
	
	public EmployeeMgmt openEmployee(String employeeName, double salary) {
		
		return this.service.createEmployee(employeeName, salary);

	}
	
	
	public void deleteEmployee(long employeeId) {
		this.service.deleteEmployee(employeeId);
	}
	
	
	public EmployeeMgmt fetchEmployeeByEmployeetId(long employeeId) throws EmployeeNotFoundException {
		return this.service.fetchEmployeeByEmployeeId(employeeId);
	}
	
	
	public Set<EmployeeMgmt> fetchEmployees() {
		return this.service.fetchEmployees();
		
	}
	

	public EmployeeMgmt fetchEmployeeByEmployeeName(String employeeName) throws EmployeeNotFoundException {
		return this.service.fetchEmployeeByEmployeeName(employeeName);
	}

	public int leaveRequest(long employeeId, int leave) throws EmployeeNotFoundException, InsufficientLeaveException {
		return this.service.leaveRequest(employeeId, leave);
		
	}

	
}
