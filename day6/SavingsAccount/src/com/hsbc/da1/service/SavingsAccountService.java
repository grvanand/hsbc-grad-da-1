package com.hsbc.da1.service;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;

import java.util.*;

public interface SavingsAccountService {
	
	SavingsAccount createSavingsAccount(String customerName, double accountBalance);
	
	SavingsAccount createSavingsAccount(String customerName, double accountBalance, String state, String city, String zipCode);

	void deleteSavingsAccount(long accountNumber);
	
	SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException;
		
	Collection<SavingsAccount> fetchSavingsAccounts();
	
	double withdraw(long accountId, double amount) throws InsufficientBalanceException, CustomerNotFoundException ;
	
	double deposit(long accountId ,double amount)throws CustomerNotFoundException;
	
	double checkBalance(long accountId)throws CustomerNotFoundException;
	
	void transfer(long fromAccountId, long toAccountId, double amount) 
			throws InsufficientBalanceException, CustomerNotFoundException ;
	
	
}
