package com.hsbc.da1.util;

import java.util.*;

import com.hsbc.da1.model.SavingsAccount;

public class HashMapDemo {
	
	public static void main(String[] args) {
		
		Map<SavingsAccount, Integer> mapOfSavingsAccount = new TreeMap<>();
		//Map<SavingsAccount, Integer> mapOfSavingsAccount = new HashMap<>();
		
		SavingsAccount deepa = new SavingsAccount("DEEPA", 56000);
		SavingsAccount veena = new SavingsAccount("VEENA", 59000);
		SavingsAccount harsh = new SavingsAccount("HARSH", 89000);
		SavingsAccount pandey = new SavingsAccount("PANDEY", 67000);
		SavingsAccount vinay = new SavingsAccount("VINAY", 34000);
		SavingsAccount kuldeep = new SavingsAccount("KULDEEP", 12000);
		
		mapOfSavingsAccount.put(deepa, 1000);
		mapOfSavingsAccount.put(veena, 1001);
		mapOfSavingsAccount.put(harsh, 1002);
		mapOfSavingsAccount.put(pandey, 1003);
		mapOfSavingsAccount.put(vinay, 1004);
		mapOfSavingsAccount.put(kuldeep, 1005);
		
		//System.out.printf("Size of the map is %d %n", mapOfSavingsAccount.size());
		//System.out.println("Is Kuldeep present : " + (mapOfSavingsAccount.containsValue(1005)));
		
		Set<Integer> setOfKeys = mapOfSavingsAccount.keySet();
		
		Iterator<Integer> keys = setOfKeys.iterator();
		
		while(keys.hasNext()) {
			int key = keys.next();
			//System.out.printf("Key is %d %s %n", key, mapOfSavingsAccount.get(key));
		}
		
		Collection<SavingsAccount> savingsAccounts = mapOfSavingsAccount.values();
		
		Iterator<SavingsAccount> its = savingsAccounts.iterator();
		
		Set<Map.Entry<SavingsAccount, Integer>> setOfEntries = mapOfSavingsAccount.entrySet();
		
		Iterator<Map.Entry<SavingsAccount, Integer>> iter = setOfEntries.iterator();
		
		while(iter.hasNext()) {
			Map.Entry<SavingsAccount, Integer> entry = iter.next();
			System.out.printf(" Key is %d and value is %s %n", entry.getKey(), entry.getValue().getCustomerName());
		}
	}
}
