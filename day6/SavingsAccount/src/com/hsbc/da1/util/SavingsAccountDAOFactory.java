package com.hsbc.da1.util;

import com.hsbc.da1.dao.ArrayBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.ListBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.dao.SetBackedSavingsAccountDAOImpl;

public class SavingsAccountDAOFactory {
	
	public static SavingsAccountDAO getSavingsAccountDAO(int choice) {
		switch (choice) {
		case 1:
			return new ArrayBackedSavingsAccountDAOImpl();
		case 2:
			return new ListBackedSavingsAccountDAOImpl();
		case 3:
			return new SetBackedSavingsAccountDAOImpl();
		default:
			return null;
		}
	}
			
}
