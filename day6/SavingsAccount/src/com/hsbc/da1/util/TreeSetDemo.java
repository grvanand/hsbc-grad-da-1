package com.hsbc.da1.util;

import java.util.*;

import com.hsbc.da1.model.SavingsAccount;

public class TreeSetDemo {
	
	public static void main(String[] args) {
		SavingsAccount deepa = new SavingsAccount("DEEPA", 56000);
		SavingsAccount veena = new SavingsAccount("VEENA", 59000);
		SavingsAccount harsh = new SavingsAccount("HARSH", 89000);
		SavingsAccount pandey = new SavingsAccount("PANDEY", 67000);
		SavingsAccount vinay = new SavingsAccount("VINAY", 34000);
		SavingsAccount kuldeep = new SavingsAccount("KULDEEP", 12000);
		
		Set<SavingsAccount> set = new TreeSet<>();
		//Set<SavingsAccount> set = new TreeSet<>();
		set.add(kuldeep);
		set.add(pandey);
		set.add(veena);
		set.add(deepa);
		set.add(vinay);
		set.add(harsh);
		
		Iterator<SavingsAccount> it = set.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}
	
	class SortSAByAccountNumberAsc implements Comparator<SavingsAccount>{

		@Override
		public int compare(SavingsAccount o1, SavingsAccount o2) {
			// TODO Auto-generated method stub
			return (int)(o1.getAccountNumber() - o2.getAccountNumber());
		}
		
	}
	
	class SortSAByAccountNumberDsc implements Comparator<SavingsAccount>{

		@Override
		public int compare(SavingsAccount o1, SavingsAccount o2) {
			// TODO Auto-generated method stub
			return -1 *(int)(o1.getAccountNumber() - o2.getAccountNumber());
		}
		
	}
	
	class SortSAByAccountNameAsc implements Comparator<SavingsAccount>{

		@Override
		public int compare(SavingsAccount o1, SavingsAccount o2) {
			// TODO Auto-generated method stub
			return o1.getCustomerName().compareTo(o2.getCustomerName());
		}
		
	}
	
	class SortSAByAccountNameDsc implements Comparator<SavingsAccount>{

		@Override
		public int compare(SavingsAccount o1, SavingsAccount o2) {
			// TODO Auto-generated method stub
			return -1 * o1.getCustomerName().compareTo(o2.getCustomerName());
		}
		
	}

}
