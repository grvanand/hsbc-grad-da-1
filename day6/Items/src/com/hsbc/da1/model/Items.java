package com.hsbc.da1.model;

import java.io.Serializable;

public class Items implements Serializable{
	
	private long itemId;
	private String itemName;
	private double itemPrice;
	
	private static long counter = 10000;
	
	public Items(String itemName, double itemPrice) {
		this.itemId = ++counter;
		this.itemName = itemName;
		this.itemPrice = itemPrice;
	}

	/**
	 * @return the itemId
	 */
	public long getItemId() {
		return itemId;
	}

	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	/**
	 * @return the itemPrice
	 */
	public double getItemPrice() {
		return itemPrice;
	}

	/**
	 * @param itemPrice the itemPrice to set
	 */
	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	
	

}
