package com.hsbc.da1.controller;

import com.hsbc.da1.service.*;
import com.hsbc.da1.model.Item;

public class ItemController {
	
	private ItemService service = new ItemServiceImpl();
	
	
	public Item newItem(String itemName, double itemPrice) {
		Item item = this.service.createItem(itemName, itemPrice);
		return item;
	}
	
	public void deleteItem(long itemId) {
		this.service.deleteItem(itemId);
	}

	
	public Item[] fetchItems() {
		Item items[] = this.service.fetchItems(); 
		return items;
	}

	
	public Item fetchItemByItemId(long itemId) {
		Item item = this.service.fetchItemByItemId(itemId);
		return item;
	}
	
	/*
	@Override
	public Item addItem(long itemId) {
		return null;
	}

	@Override
	public Item removeItem(long itemId) {
		return null;
	}
	*/

}
