package com.hsbc.da1.service;

import com.hsbc.da1.model.Item;

public interface ItemService {
	
	Item createItem(String itemName, double itemPrice);
	//Item updateItem(long itemId, Item localItem);
	void deleteItem(long itemId);
	Item[] fetchItems();
	Item fetchItemByItemId(long itemId);
	
	//Item addItem(long itemId);
	//Item removeItem(long itemId);

}
