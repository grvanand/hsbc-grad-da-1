package com.hsbc.da1.service;

import com.hsbc.da1.model.Item;
import com.hsbc.da1.dao.ItemDAO;
import com.hsbc.da1.dao.ItemDAOImpl;

public class ItemServiceImpl implements ItemService{

	private ItemDAO dao = new ItemDAOImpl();
		
	@Override
	public Item createItem(String itemName, double itemPrice) {
		Item localItem = new Item(itemName , itemPrice);
		Item itemCreated = this.dao.saveItem(localItem);
		return itemCreated;
	}

	

	@Override
	public void deleteItem(long itemId) {
		this.dao.deleteItem(itemId);
	}

	@Override
	public Item[] fetchItems() {
		Item items[] = this.dao.fetchItems(); 
		return items;
	}

	@Override
	public Item fetchItemByItemId(long itemId) {
		Item item = this.dao.fetchItemByItemId(itemId);
		return item;
	}
	
	/*
	@Override
	public Item addItem(long itemId) {
		return null;
	}

	@Override
	public Item removeItem(long itemId) {
		return null;
	}
	*/

}
