package com.hsbc.da1.model;

public class Item {

	private static long counter = 1000;
	
	private String itemName;
	private double itemPrice;
	private long itemId;
	
	public Item(String itemName, double itemPrice) {
		this.itemId = ++counter;
		this.itemName = itemName;
		this.itemPrice = itemPrice;
	}

	public String getItemName(long itemId) {
		return itemName;
	}

	public double getItemPrice(long itemId) {
		return itemPrice;
	}

	public long getItemId() {
		return itemId;
	}

	@Override
	public String toString() {
		return "Item [itemName = " + itemName + ", itemPrice = " + itemPrice + ", itemId = " + itemId + "]";
	}
	
	
}
