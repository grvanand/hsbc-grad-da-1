package com.hsbc.da1.dao;

import com.hsbc.da1.model.Item;

public interface ItemDAO {
	
	Item saveItem(Item localItem);
	Item updateItem(long itemId, Item localItem);
	void deleteItem(long itemId);
	Item[] fetchItems();
	Item fetchItemByItemId(long itemId);

}
