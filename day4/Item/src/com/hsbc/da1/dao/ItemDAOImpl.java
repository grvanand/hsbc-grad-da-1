package com.hsbc.da1.dao;

import com.hsbc.da1.model.Item;

public class ItemDAOImpl implements ItemDAO {

	private static Item[] items = new Item[100];
	private static int counter;
	
	
	@Override
	public Item saveItem(Item localItem) {
		items[counter++] = localItem;
		return localItem;
	}

	@Override
	public Item updateItem(long itemId, Item localItem) {
		for(int index = 0; index <= items.length; index++) {
			if(itemId == items[index].getItemId()) {
				items[index] = localItem;
				break;
			}
		}
		return localItem;
	}

	@Override
	public void deleteItem(long itemId) {
		for(int index = 0; index <= items.length; index++) {
			if(itemId == items[index].getItemId()) {
				items[index] = null;
				break;
			}
		}
		
		
	}

	@Override
	public Item[] fetchItems() {
		return items;
	}

	@Override
	public Item fetchItemByItemId(long itemId) {
		for(int index = 0; index <= items.length; index++) {
			if(itemId == items[index].getItemId()) {
				return items[index];
			}
		}
		return null;
	}

}

