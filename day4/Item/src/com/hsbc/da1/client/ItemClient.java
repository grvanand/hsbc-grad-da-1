package com.hsbc.da1.client;

import com.hsbc.da1.controller.ItemController;
import com.hsbc.da1.model.Item;

public class ItemClient {

		public static void main(String args[]) {
			
			ItemController controller = new ItemController();
			
			Item commodity = controller.newItem("Maggi", 20.0);
			
			System.out.println("Item Id : " + commodity.getItemId());
		}
}
