package com.hsbc.da1.dao;

import java.util.*;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class ListBackedSavingsAccountDAOImpl implements SavingsAccountDAO{
	
	private List<SavingsAccount> savingsAccountList = new ArrayList<>();
	
	public SavingsAccount saveSavingsAccount(SavingsAccount localSavingsAccount){
		this.savingsAccountList.add(localSavingsAccount);
		return localSavingsAccount;		
	}
	
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount localSavingsAccount) {
		for(SavingsAccount savingsAccount : savingsAccountList) {
			if(savingsAccount != null && savingsAccount.getAccountNumber() == accountNumber) {
				savingsAccount = localSavingsAccount;
			}
		}
		return localSavingsAccount;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		for(SavingsAccount savingsAccount : savingsAccountList) {
			if(savingsAccount != null && savingsAccount.getAccountNumber() == accountNumber) {
				this.savingsAccountList.remove(savingsAccount);
			}
		}
	}
	
	public SavingsAccount[] fetchSavingsAccounts() {
		return (SavingsAccount[]) savingsAccountList.toArray();
	}
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		for(SavingsAccount savingsAccount : savingsAccountList) {
			if(savingsAccount != null && savingsAccount.getAccountNumber() == accountNumber) {
				return savingsAccount;
			}
		}
		return null;
	}

}
