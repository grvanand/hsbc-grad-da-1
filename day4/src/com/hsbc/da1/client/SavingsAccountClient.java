package com.hsbc.da1.client;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.*;

public class SavingsAccountClient {
	
	public static void main(String[] args) throws InsufficientBalanceException, CustomerNotFoundException {
		
		SavingsAccountController controller = new SavingsAccountController();
		
		SavingsAccount user1 = controller.openSavingsAccount("Rohan", 25_000, "Odisha", "Angul", "100101");
		SavingsAccount user2 = controller.openSavingsAccount("Joshi", 50_000);
		
		try {
			controller.transfer(user1.getAccountNumber(), user2.getAccountNumber(), 5_000);
		
			System.out.println("Account Id "+ user1.getAccountNumber());
			System.out.println("Account Id "+ user2.getAccountNumber());
			
			System.out.println("--------------------------------------------------------");
			
			double newAccountBalance = controller.deposit(1001, 20_000);
			System.out.println("Account Balance: " + newAccountBalance);
			
			System.out.println("--------------------------------------------------------");
		
		} catch (InsufficientBalanceException | CustomerNotFoundException exception) {
			System.out.println(exception.getMessage());	
		}
		
		SavingsAccount[] savingsAccounts = controller.fetchSavingsAccounts();
		
		for(SavingsAccount savingsAccount: savingsAccounts) {
			if ( savingsAccount != null) {
				System.out.println("Account Id : "+ savingsAccount.getAccountNumber());
				System.out.println("Account Name : "+ savingsAccount.getCustomerName());
			}
		}
		
		System.out.println("--------------------------------------------------------");
		
		for(SavingsAccount savingsAccount: savingsAccounts) {
			if ( savingsAccount != null) {
				System.out.println("Account : " + savingsAccount);
			}
		}
		
		System.out.println("--------------------------------------------------------");
	}
}
