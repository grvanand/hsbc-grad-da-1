package com.hsbc.da1.model;

public class SavingsAccount {
	
	private String customerName;
	private long accountNumber;
	private double accountBalance;
	private Address address;
	
	private static long counter = 1000;
	
	public SavingsAccount(String customerName, double accountBalance) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.accountNumber = ++counter;
		
	}
	
	public SavingsAccount(String customerName, double accountBalance, Address address) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.address = address;
		this.accountNumber = ++counter;
		
	}


	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}
	
	//@Override
	public String toString() {
		if (address != null) {
			return "SavingsAccount [customerName = " + customerName + ", accountNumber = " + accountNumber + ", accountBalance = "
					+ accountBalance + ", address = " + address.getState()+ " " + address.getCity() + " " + address.getZipCode() + "]";
		} else {
			return "SavingsAccount [customerName = " + customerName + ", accountNumber = " + accountNumber + ", accountBalance = "
					+ accountBalance + ", address = " + address + "]";
		}
	}

	
	
}
