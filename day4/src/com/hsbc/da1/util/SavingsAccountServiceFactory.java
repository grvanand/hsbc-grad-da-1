package com.hsbc.da1.util;

import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.service.SavingsAccountServiceImpl;

public class SavingsAccountServiceFactory {

	public static SavingsAccountService getInstance() {
		return new SavingsAccountServiceImpl();
	}
}
