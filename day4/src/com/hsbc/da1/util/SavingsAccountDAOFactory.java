package com.hsbc.da1.util;

import com.hsbc.da1.dao.ArrayBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.ListBackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;

public class SavingsAccountDAOFactory {
	
	public static SavingsAccountDAO getSavingsAccountDAO(int value) {
		if(value == 1) {
			return new ArrayBackedSavingsAccountDAOImpl();
		} else {
			return new ListBackedSavingsAccountDAOImpl();
		}
	}
}
