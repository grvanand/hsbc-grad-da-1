package com.hsbc.da1.controller;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.*;
import com.hsbc.da1.util.SavingsAccountServiceFactory;

public class SavingsAccountController {
	

	private SavingsAccountService savingsAccountService = SavingsAccountServiceFactory.getInstance();
	

	public SavingsAccount openSavingsAccount(String customerName, double accountBalance) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, accountBalance);	
		return savingsAccount;
	}//open account
	
	
	public SavingsAccount openSavingsAccount(String customerName, double accountBalance, 
			String state, String city, String zipCode) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, 
				accountBalance, state, city, zipCode);	
		return savingsAccount;
	}//open account

	
	public void deleteSavingsAccount(long accountNumber) {
		this.savingsAccountService.deleteSavingsAccount(accountNumber);
	}//delete account
	

	public SavingsAccount[] fetchSavingsAccounts() {
		SavingsAccount[] accounts = this.savingsAccountService.fetchSavingsAccounts();
		return accounts;
	}//get account
	

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber)  throws CustomerNotFoundException{
		SavingsAccount savingsAccount = this.savingsAccountService.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}//get account number
	

	public double withdraw(long accountId, double amount) throws InsufficientBalanceException, CustomerNotFoundException {
		return this.savingsAccountService.withdraw(accountId, amount);
	}//withdrawal from account

	
	public double deposit(long accountId, double amount)throws CustomerNotFoundException {
		return this.savingsAccountService.deposit(accountId, amount);
	}//deposit into account
	

	public double checkBalance(long accountId) throws CustomerNotFoundException {
		return this.savingsAccountService.checkBalance(accountId);
	}//balance check
	
	
	public void transfer(long senderId, long toId, double amount) 
			throws InsufficientBalanceException, CustomerNotFoundException {
		this.savingsAccountService.transfer(senderId,  toId,  amount);
	}//transfer amount
	
}
