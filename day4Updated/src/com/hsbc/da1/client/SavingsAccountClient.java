package com.hsbc.da1.client;

import java.util.*;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.*;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.util.*;

public class SavingsAccountClient {
	
	public static void main(String[] args) throws InsufficientBalanceException, CustomerNotFoundException {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter your choice as per following :");
		System.out.println("Press 1 - Array Backed");
		System.out.println("Press 2 - List Backed");
		System.out.println("Press 3 - Set Backed");
		
		System.out.println("====================================================");
		
		int choice = scanner.nextInt();
		
		System.out.println("====================================================");
		
		SavingsAccountDAO dao = SavingsAccountDAOFactory.getSavingsAccountDAO(choice);
		
		if(dao == null) {
			System.out.println("Invalid Choice! Try again..");
			return;
		}
		
		SavingsAccountService service = SavingsAccountServiceFactory.getInstance(dao);
		
		SavingsAccountController controller = new SavingsAccountController(service);
		
		SavingsAccount user1 = controller.openSavingsAccount("Rohan", 25_000, "Odisha", "Angul", "100101");
		SavingsAccount user2 = controller.openSavingsAccount("Joshi", 50_000);
		
		try {
			controller.transfer(56453, user2.getAccountNumber(), 5_000);
			
			System.out.println("Account Id "+ user1.getAccountNumber());
			System.out.println("Account Id "+ user2.getAccountNumber());
			System.out.println("====================================================");
			
			double newAccountBalance = controller.deposit(1001, 20_000);
			System.out.println("Account Balance: " + newAccountBalance);
			System.out.println("====================================================");
		
		} catch (InsufficientBalanceException | CustomerNotFoundException exception) {
			System.out.println(exception.getMessage());	
		}
		
		Collection<SavingsAccount> savingsAccountList = controller.fetchSavingsAccounts();
		
		for(SavingsAccount savingsAccount: savingsAccountList) {
			if ( savingsAccount != null) {
				System.out.println("Account Id : "+ savingsAccount.getAccountNumber());
				System.out.println("Account Name : "+ savingsAccount.getCustomerName());
			}
		}
		
		System.out.println("====================================================");
		
		for(SavingsAccount savingsAccount: savingsAccountList) {
			if ( savingsAccount != null) {
				System.out.println("Account : " + savingsAccount);
			}
		}
		
		System.out.println("====================================================");
	}
}
