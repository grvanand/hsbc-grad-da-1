package com.hsbc.da1.model;

public class SavingsAccount {
	
	private String customerName;
	private long accountNumber;
	private double accountBalance;
	private Address address;
	
	private static long counter = 1000;
	
	public SavingsAccount(String customerName, double accountBalance) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.accountNumber = ++counter;
		
	}
	
	public SavingsAccount(String customerName, double accountBalance, Address address) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.address = address;
		this.accountNumber = ++counter;
		
	}


	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	//@Override
	public String toString() {
		if (address != null) {
			return "SavingsAccount [customerName = " + customerName + ", accountNumber = " + accountNumber + ", accountBalance = "
					+ accountBalance + ", address = " + address.getState()+ " " + address.getCity() + " " + address.getZipCode() + "]";
		} else {
			return "SavingsAccount [customerName = " + customerName + ", accountNumber = " + accountNumber + ", accountBalance = "
					+ accountBalance + ", address = " + address + "]";
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (accountNumber ^ (accountNumber >>> 32));
		result = prime * result + ((customerName == null) ? 0 : customerName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof SavingsAccount))
			return false;
		SavingsAccount other = (SavingsAccount) obj;
		if (accountNumber != other.accountNumber)
			return false;
		if (customerName == null) {
			if (other.customerName != null)
				return false;
		} else if (!customerName.equals(other.customerName))
			return false;
		return true;
	}

}
