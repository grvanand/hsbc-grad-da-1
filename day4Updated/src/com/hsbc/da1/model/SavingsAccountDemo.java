package com.hsbc.da1.model;

import java.util.*;

public class SavingsAccountDemo {

		public static void main(String[] args) {
			
			SavingsAccount sa = new SavingsAccount("Dinesh", 3500);
			sa.setAccountNumber(1234);
			
			SavingsAccount dinesh = new SavingsAccount("Dinesh", 3500);
			dinesh.setAccountNumber(12345);
			
			Set<SavingsAccount> savingsAccountSet = new HashSet<>();
			
			savingsAccountSet.add(dinesh);
			savingsAccountSet.add(sa);
			
			SavingsAccount dineshh = new SavingsAccount("dinesh", 35000);
			dinesh.setAccountNumber(12345678);
			savingsAccountSet.add(dineshh);
			
			Iterator<SavingsAccount> it = savingsAccountSet.iterator();
			
			while(it.hasNext()) {
				System.out.println(it.next());
			}
			
			
		}
}
