package com.hsbc.da1.service;

import com.hsbc.da1.dao.*;
import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.model.Address;
import java.util.*;

public final class SavingsAccountServiceImpl implements SavingsAccountService {
	

	private SavingsAccountDAO dao ;
	
	public SavingsAccountServiceImpl(SavingsAccountDAO dao) {
		this.dao = dao;
	}
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance){
		//no validations
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance);
		
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
		return savingsAccountCreated;
	}//creating the account
	
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, String state, String city, String zipCode){
		//no validations
		Address address = new Address(state, city, zipCode);
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance, address);
		
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
		return savingsAccountCreated;
	}//creating the account
	
	
	public void deleteSavingsAccount(long accountNumber){
		this.dao.deleteSavingsAccount(accountNumber);
	}//deleting the account
	
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException{
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}//getting account number
	
	
	public Collection<SavingsAccount> fetchSavingsAccounts() {
		return this.dao.fetchSavingsAccounts();
		
	}//getting the account
	
	
	public double withdraw(long accountId, double amount) 
			throws InsufficientBalanceException, CustomerNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if(savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			if( currentAccountBalance >= amount) {
				currentAccountBalance = currentAccountBalance - amount;
				savingsAccount.setAccountBalance(currentAccountBalance);
				this.dao.updateSavingsAccount(accountId, savingsAccount);
				return amount;
			} else {
				throw new InsufficientBalanceException("Insufficient Balance");
			}
		}
		return 0;
	}//withdrawal from account
	
	
	public double deposit(long accountId ,double amount) throws CustomerNotFoundException{
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if(savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			savingsAccount.setAccountBalance(currentAccountBalance + amount);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}//deposit into account
	
	
	public double checkBalance(long accountId)throws CustomerNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if(savingsAccount != null) {
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}//balance check
	
	
	public void transfer(long fromAccountId, long toAccountId, double amount) 
			throws InsufficientBalanceException, CustomerNotFoundException  {
		SavingsAccount fromAccount = this.dao.fetchSavingsAccountByAccountId(fromAccountId);
		SavingsAccount toAccount = this.dao.fetchSavingsAccountByAccountId(toAccountId);
		if(fromAccount != null && toAccount != null) {
			double netBalance = this.withdraw(fromAccount.getAccountNumber(), amount);
			if(netBalance != 0) {
				this.deposit(toAccount.getAccountNumber(), amount);
			}
		}
	}//transfer amount from one account to another

}
