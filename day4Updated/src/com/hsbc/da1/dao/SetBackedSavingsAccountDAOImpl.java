package com.hsbc.da1.dao;

import java.util.*;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public final class SetBackedSavingsAccountDAOImpl implements SavingsAccountDAO {
	
	private Set<SavingsAccount> savingsAccountSet = new HashSet<>();
		
		public SavingsAccount saveSavingsAccount(SavingsAccount localSavingsAccount){
			this.savingsAccountSet.add(localSavingsAccount);
			return localSavingsAccount;		
		}
		
		public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount localSavingsAccount) {
			for(SavingsAccount savingsAccount : savingsAccountSet) {
				if(savingsAccount != null && savingsAccount.getAccountNumber() == accountNumber) {
					savingsAccount = localSavingsAccount;
				}
			}
			return localSavingsAccount;
		}
		
		public void deleteSavingsAccount(long accountNumber) {
			for(SavingsAccount savingsAccount : savingsAccountSet) {
				if(savingsAccount != null && savingsAccount.getAccountNumber() == accountNumber) {
					this.savingsAccountSet.remove(savingsAccount);
				}
			}
		}
		
		public Collection<SavingsAccount> fetchSavingsAccounts() {
			return this.savingsAccountSet;
		}
		
		public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
			for(SavingsAccount savingsAccount : savingsAccountSet) {
				if(savingsAccount != null && savingsAccount.getAccountNumber() == accountNumber) {
					return savingsAccount;
				}
			}
			throw new CustomerNotFoundException("No customer found");
		}

}
