package com.hsbc.da1.dao;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

import java.util.*;

public interface SavingsAccountDAO {
	
	SavingsAccount saveSavingsAccount(SavingsAccount localSavingsAccount);
	
	SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount localSavingsAccount);
		
	void deleteSavingsAccount(long accountNumber);
	
	Collection<SavingsAccount> fetchSavingsAccounts();
		
	SavingsAccount fetchSavingsAccountByAccountId(long accountNumber)throws CustomerNotFoundException;
		

}
